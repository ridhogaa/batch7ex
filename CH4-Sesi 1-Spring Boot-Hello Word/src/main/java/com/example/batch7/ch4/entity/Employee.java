package com.example.batch7.ch4.entity;

import lombok.Data;

@Data
public class Employee {
    private Long id;
    private  String name;
    private String gender;
}
